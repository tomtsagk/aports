# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.47.0
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86 !ppc64le !riscv64" # FTBFS on 32-bit arches, riscv64, ppc64le
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -ldflags "
		-X github.com/anchore/syft/internal/version.version=$pkgver
		" \
		-o bin/syft ./cmd/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft "$pkgdir"/usr/bin/syft
}

sha512sums="
997d6e7139be8c686ee38df9fe42fa50dac17e522c51db005628cf3fd6a97ef9f9758e86fe6c6e58a199d852bc4eeac88b3932c5f3847547382987cd02260551  syft-0.47.0.tar.gz
"
