# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=prettier
pkgver=2.7.0
pkgrel=0
pkgdesc="Opinionated code formatter"
url="https://prettier.io/"
license="MIT"
# riscv64: blocked by nodejs
# armhf, armv7, x86: fails to build on 32-bit arches
arch="noarch !armhf !armv7 !riscv64 !x86"
depends="nodejs"
makedepends="yarn"
checkdepends="npm"
subpackages="$pkgname-doc"
source="https://github.com/prettier/prettier/archive/$pkgver/prettier-$pkgver.tar.gz
	timeout.patch
	"

build() {
	yarn install --frozen-lockfile
	yarn build
}

check() {
	yarn test:dist
}

package() {
	local destdir=/usr/lib/node_modules/prettier

	install -d \
		"$pkgdir"/usr/bin \
		"$pkgdir"/$destdir \
		"$pkgdir"/usr/share/licenses/prettier

	cp -r dist/* "$pkgdir"/$destdir
	ln -s $destdir/bin-prettier.js "$pkgdir"/usr/bin/prettier

	cd "$pkgdir"/$destdir
	rm -r esm # remove ES modules: not needed for command-line usage
	rm README.md # more links to various badges than actual content
	mv LICENSE "$pkgdir"/usr/share/licenses/prettier/LICENSE
}

sha512sums="
2ce31ba3cde2b355224a9121bc37110ea9bb8bfcd1d25099bf28e7ee8d1d78503a6fdb2ff0f4d61097186cffeefc594ba28f710d16f13795de5e8bdadc5e3c7a  prettier-2.7.0.tar.gz
06968fc076d2cd68360601c84c49b82a1f872a1b54d5ec9738de7ac63793f96cd609817bbb0a0f1902b7ba004232d1276b3577557dc60dd2a7d71fad5e440099  timeout.patch
"
