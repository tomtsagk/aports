# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gst-editing-services
pkgver=1.20.2
pkgrel=0
pkgdesc="GStreamer Editing Services Library"
url="https://gstreamer.freedesktop.org"
# s390x blocked by 7 failing tests
arch="all !s390x"
license="LGPL-2.0-or-later"
makedepends="gstreamer-dev gtk-doc python3 gobject-introspection-dev py3-gobject3-dev
	glib-dev gst-plugins-good gst-plugins-bad-dev gst-plugins-base-dev
	libxml2-dev flex meson"
subpackages="$pkgname-dev $pkgname-doc"
source="https://gstreamer.freedesktop.org/src/gst-editing-services/gst-editing-services-$pkgver.tar.xz"
options="!check" # https://gitlab.freedesktop.org/gstreamer/gst-editing-services/-/issues/125

build() {
	abuild-meson \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
a9f9dd196d838fab8727de6fd0c52fde6a1b56ba801b7b608e0f2d30abc38bf586f02c20bedcb1ba384089cf97e8d0db1719ed1c9e8c18c7e56ff732bb8e5de5  gst-editing-services-1.20.2.tar.xz
"
